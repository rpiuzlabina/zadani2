import RPi.GPIO as GPIO
from time import sleep
GPIO.setmode(GPIO.BCM)
GPIO.setup(2,GPIO.OUT)
GPIO.setup(3, GPIO.IN, pull_up_down=GPIO.PUD_UP)
try:
    while(1):
        GPIO.wait_for_edge(3, GPIO.RISING)
        GPIO.output(2, False) if GPIO.input(2) == True  else GPIO.output(2, True)
except:
    pass
finally:
    GPIO.cleanup()
