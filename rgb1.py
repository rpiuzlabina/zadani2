import RPi.GPIO as GPIO
from time import sleep
GPIO.setmode(GPIO.BCM)
pins = {
2,3,4
}
for pin in pins:
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, GPIO.LOW)
pwm1 = GPIO.PWM(2, 100)
pwm2 = GPIO.PWM(3, 100)
pwm3 = GPIO.PWM(4, 100)
pwm1.start(0)
pwm2.start(0)
pwm3.start(0)
try:
    while 1:
        for i in range(100):
            pwm1.ChangeDutyCycle(i)
            for l in range(100):
                pwm1.ChangeDutyCycle(100-i)
                pwm2.ChangeDutyCycle(l)
                sleep(0.05)
                for m in range(100):
                    pwm3.ChangeDutyCycle(m)
                    sleep(0.05)
            sleep(0.05)
except:
    pass
finally:
    pwm1.stop()
    pwm2.stop()
    pwm3.stop()
    GPIO.cleanup()
