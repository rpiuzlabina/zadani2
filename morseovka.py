# -*- coding: utf-8 -*-
import RPi.GPIO as GPIO
import requests as request
from time import sleep
GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT)
id = 'PniWh1TCtelz6G8iihIlb7ykBGSKKiKH'
url = 'https://ioe.zcu.cz/morse.php?id='
print("Zahajuji request na adresu: "+url+id)
try:
    r = request.get(url+id) #get request morse
except request.exceptions.RequestException as e:
    sys.exit(1)
#print(r.text)
else:
    print("Dotaz proběhl úspěšně \n")
    inp = 0
    text = r.text
    dot = 0.02
    asciilist = {
	'.-':'A',
	'-...':'B',
	'-.-.':'C',
	'-..':'D',
	'.':'E',
	'..-.':'F',
	'--.':'G',
	'....':'H',
	'----':'CH',
        '..':'I',
	'.---':'J',
	'-.-':'K',
	'.-..':'L',
	'--':'M',
	'-.':'N',
	'---':'O',
	'.--.':'P',
	'--.-':'Q',
	'.-.':'R',
	'...':'S',
	'-':'T',
	'..-':'U',
	'...-':'V',
	'.--':'W',
	'-..-':'X',
	'-.--':'Y',
	'--..':'Z',
	'.----':'1',
	'..---':'2',
	'...--':'3',
	'....-':'4',
	'.....':'5',
	'-....':'6',
	'--...':'7',
	'---..':'8',
	'----.':'9',
	'-----':'0',
	'..--..':'?',
	'--..--':',',
	'--...-':'!',
	'.-.-.-':'.',
	'-.--.':'(',
	'-.--.-':')',
	'---...':':',
	'':''
    }
    pismeno = ""
    slovo = ""
    komplet = ""
    try:
        for char in text:
            if(char == "-"):
                sl = dot*2  
                inp = 1
                pismeno = pismeno+char
                print("-")
            elif(char == "."):
                sl = dot
                inp = 1
                pismeno = pismeno+char
                print(".")
            elif(char == " "):
                sl = dot*3
                inp = 0
                slovo = slovo+asciilist[pismeno]
                pismeno = ""
                print("konec písmena "+slovo)
            elif(char == "/"):
                sl = dot*5
                inp = 0
                komplet = komplet+" "+slovo
                slovo = ""
                print("konec slova "+komplet)
            GPIO.output(18, inp)
            sleep(sl)
            GPIO.output(18, 0)
            sleep(dot)
        komplet = komplet+" "+slovo #zapsání posledního slova
        print("Konec scriptu.Zde je výsledek: "+komplet)
    except KeyboardInterrupt:
        print("Script předčasně ukončen. Zde je aspoň částečný překlad: "+komplet)
    except:
        print("Vyskytla se neočekávaná chyba. Zde je aspon částečný překlad: "+komplet)
    finally:
        GPIO.cleanup()


