import RPi.GPIO as GPIO
from time import sleep
GPIO.setmode(GPIO.BCM)
GPIO.setup(2, GPIO.OUT)
GPIO.setup(3, GPIO.IN, pull_up_down=GPIO.PUD_UP)
s = 1
try:
    while(1):
        if(GPIO.input(3) == False):
            print("1")
            GPIO.output(2, True)
            sleep(s)
            GPIO.output(2, False)
finally:
    GPIO.cleanup()
